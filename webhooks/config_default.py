import os
from webhooks import app

SERVER_NAME = 'localhost:1234'

DEBUG = True

SECRET_KEY = 'set-your-secret-key'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.abspath(
    os.path.join(app.instance_path, 'db.sqlite3')
)

SQLALCHEMY_TRACK_MODIFICATIONS = False

RSS_ITEMS_COUNT = 10
