import io
import json
import os
import re

from flask import abort, redirect, render_template, request, send_file, url_for
from flask_login import LoginManager, login_required, login_user, logout_user
from werkzeug.security import check_password_hash

from webhooks import app, db
from webhooks.api import GitLabAPI
from webhooks.forms import LoginForm, PipelineHookForm, ProjectAddForm
from webhooks.models import PipelineHook, PipelineHookEvent, Project, User
from webhooks.processing import processing_hook
from webhooks.rss import create_startup_pipeline_hook_xml

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/')
@login_required
def index():
    projects = Project.query.all()
    return render_template('index.html', title='Projects', projects=projects)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if check_password_hash(user.password_hash, form.password.data):
                login_user(user, remember=form.remember_me.data)
                return redirect(url_for('index'))
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/rss')
def rss():
    pipeline_hook_events = PipelineHookEvent.query.all()
    events = []
    for event in pipeline_hook_events:
        link = url_for(
            'rss_xml',
            project_name=event.pipeline_hook.project.name,
            branch=event.pipeline_hook.branch,
            filename=f'{event.pipeline_hook.status}_rss.xml'
        )
        events.append(
            {
                'project': event.pipeline_hook.project.name,
                'branch': event.pipeline_hook.branch,
                'status': event.pipeline_hook.status,
                'last_update': event.date.strftime('%a, %d %b %Y %H:%M:%S GMT'),
                'link': link
            }
        )
    return render_template('rss.html', title='RSS', events=events)


@app.route('/rss/<project_name>/<branch>/<filename>')
def rss_xml(project_name, branch, filename):
    filepath = os.path.join(
        app.instance_path,
        'rss',
        project_name,
        branch,
        filename
    )
    response = send_file(
        filepath,
        attachment_filename=filename,
        as_attachment=True
    )
    response.headers['Content-Length'] = response.content_length
    response.headers['Cache-Control'] = 'no-cache'
    return response


@app.route('/projects/add', methods=['GET', 'POST'])
@login_required
def projects_add():
    form = ProjectAddForm()
    if form.validate_on_submit():
        api = GitLabAPI(
            personal_access_token=form.personal_access_token.data,
            web_url=form.url.data
        )
        project_data = api.get_single_project_from_url(form.url.data)

        try:
            new_project = Project(
                project_id=project_data.get('id'),
                name=project_data.get('name'),
                name_with_namespace=project_data.get('name_with_namespace'),
                description=project_data.get('description'),
                web_url=project_data.get('web_url'),
                personal_access_token=form.personal_access_token.data,
                api_url=api.api_url
            )

            db.session.add(new_project)
            db.session.commit()

            return redirect(url_for('index'))
        except Exception:
            abort(404)

    return render_template('projects_add.html', title='Project add', form=form)


@app.route('/project/<id>')
@login_required
def project_detail(id):
    project = Project.query.filter_by(id=id).first()
    if project:
        return render_template(
            'project_detail.html',
            title=project.name_with_namespace,
            project=project,
            pipeline_hooks=project.pipeline_hooks.all()
        )
    abort(404)


@app.route('/project/<id>/webhooks/add/pipeline', methods=['GET', 'POST'])
@login_required
def project_webhooks_add_pipeline(id):
    project = Project.query.filter_by(id=id).first()
    if project:
        api = GitLabAPI(
            api_url=project.api_url,
            personal_access_token=project.personal_access_token
        )
        branches = api.list_repository_branches(project.project_id)

        form = PipelineHookForm()
        form.branch.choices = [(branch, branch) for branch in branches]
        if form.validate_on_submit():
            try:
                new_pipeline_hook = PipelineHook(
                    branch=form.branch.data,
                    status=form.status.data,
                    service=form.service.data,
                    filename=form.filename.data,
                    download_filename=form.download_filename.data,
                    version_filename=form.version_filename.data,
                    secret_token=form.secret_token.data,
                    project_id=id
                )

                db.session.add(new_pipeline_hook)
                db.session.commit()

                create_startup_pipeline_hook_xml(new_pipeline_hook)

                event = PipelineHookEvent(
                    pipeline_hook_id=new_pipeline_hook.id
                )

                db.session.add(event)
                db.session.commit()

                return redirect(url_for('project_detail', id=id))
            except Exception as exc:
                print(exc)
                abort(404)

        return render_template(
            'webhooks_add_pipeline.html',
            title=project.name_with_namespace + ' - add pipeline hook',
            project=project,
            form=form
        )
    abort(404)


@app.route('/download/rss/<pipeline_hook_id>/<sha>')
def download_rss(pipeline_hook_id, sha):
    pipeline_hook = PipelineHook.query.filter_by(
        id=pipeline_hook_id
    ).first()
    if not pipeline_hook:
        abort(404)

    api = GitLabAPI(
        api_url=pipeline_hook.project.api_url,
        personal_access_token=pipeline_hook.project.personal_access_token
    )

    try:
        data = api.get_raw_file_from_repository(
            pipeline_hook.project.project_id, pipeline_hook.filename, ref=sha
        )
    except Exception:
        abort(404)

    version = []
    if pipeline_hook.version_filename:
        version_data = api.get_raw_file_from_repository(
            pipeline_hook.project.project_id,
            pipeline_hook.version_filename,
            ref=sha
        )
        if version_data:
            regex = r'\d{1,2}.\d{1,2}.\d{1,2}'
            version_string = version_data.decode("utf-8")
            version = re.findall(regex, version_string)

    filename = pipeline_hook.download_filename if pipeline_hook.download_filename else pipeline_hook.filename
    if len(version):
        filename = f'{filename}_{version[0]}'

    ext = pipeline_hook.filename.split('.')[-1]
    filename = f'{filename}.{ext}'

    response = send_file(
        io.BytesIO(data),
        attachment_filename=filename,
        as_attachment=True
    )
    response.headers['Content-Length'] = response.content_length
    response.headers['Cache-Control'] = 'no-cache'
    return response


@app.route('/webhooks', methods=['GET', 'POST'])
def webhooks():
    if request.method == 'POST':
        if 'X-Gitlab-Event' not in request.headers:
            abort(404)

        hook_type = request.headers.get('X-Gitlab-Event', '')
        token = request.headers.get('X-Gitlab-Token', '')

        try:
            data = json.loads(request.data)
        except Exception:
            abort(415)

        processing_hook(hook_type, data, token=token)

    return 'ok'
