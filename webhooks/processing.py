from datetime import datetime

from webhooks import db, rss
from webhooks.models import PipelineHook, PipelineHookEvent, Project


def processing_hook(hook_type, data, token=None):
    if hook_type == 'Pipeline Hook':
        processing_pipeline_hook(data, token)
    else:
        raise NotImplementedError


def processing_pipeline_hook(data, token):
    project_id = data['project']['id']
    branch = data['object_attributes']['ref']
    status = data['object_attributes']['status']

    project = Project.query.filter_by(project_id=project_id).first()
    if not project:
        return

    pipeline_hook = PipelineHook.query.filter_by(
        project_id=project.id,
        branch=branch,
        status=status
    ).first()

    if pipeline_hook:
        if pipeline_hook.service == 'RSS creation':
            rss.update_pipeline_hook_xml(pipeline_hook, data, token)

            event = PipelineHookEvent.query.filter_by(
                pipeline_hook_id=pipeline_hook.id
            ).first()

            if not event:
                event = PipelineHookEvent(
                    pipeline_hook_id=pipeline_hook.id
                )
            else:
                event.date = datetime.utcnow()

            db.session.add(event)
            db.session.commit()
