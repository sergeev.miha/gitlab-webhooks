from flask_wtf import FlaskForm
from wtforms import (BooleanField, PasswordField, SelectField, StringField,
                     SubmitField)
from wtforms.validators import URL, Email, InputRequired, Length


class LoginForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[
            InputRequired(),
            Email(message='Invalid email'),
            Length(max=50)
        ]
    )
    password = PasswordField(
        'Password',
        validators=[InputRequired(), Length(min=1, max=80)]
    )
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Log in')


class ProjectAddForm(FlaskForm):
    url = StringField(
        'Url',
        validators=[InputRequired(), URL(message='Invalid url')]
    )
    personal_access_token = StringField(
        'Personal access token',
        validators=[InputRequired(), Length(max=50)]
    )
    submit = SubmitField('Add')


class PipelineHookForm(FlaskForm):
    _choices = [
        ('running', 'running'),
        ('pending', 'pending'),
        ('success', 'success'),
        ('failed', 'failed'),
        ('canceled', 'canceled'),
        ('skipped', 'skipped')
    ]

    branch = SelectField('Branch', choices=[])
    status = SelectField('Status', choices=_choices)
    service = SelectField(
        'Service',
        choices=[('RSS creation', 'RSS creation')]
    )
    filename = StringField(
        'Filename (only Service is RSS creation)',
        validators=[Length(max=50)]
    )
    download_filename = StringField(
        'Download filename (only Service is RSS creation)',
        validators=[Length(max=50)]
    )
    version_filename = StringField(
        'Path to version filename (only Service is RSS creation)',
        validators=[Length(max=128)]
    )
    secret_token = StringField('Secret token', validators=[Length(max=128)])
    submit = SubmitField('Add')
