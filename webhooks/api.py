from urllib.parse import urlparse, quote

import requests


class GitLabAPI:
    ''' Class to work with GitLab API'''

    def __init__(self, api_url=None, personal_access_token=None, web_url=None):
        self.api_url = api_url or 'https://gitlab.com/api/v4'
        self.personal_access_token = personal_access_token
        self.web_url = web_url
        if self.web_url:
            parsed_url = urlparse(web_url)
            self.api_url = f'{parsed_url.scheme}://{parsed_url.netloc}/api/v4'

    def _get(self, url, return_json=True, return_bytes=False):
        try:
            headers = {'Private-Token': self.personal_access_token}
            response = requests.get(self.api_url + url, headers=headers)
        except requests.exceptions as exc:
            print(exc)
            raise

        if return_json:
            return response.json()

        if return_bytes:
            return response.content

        return response.text

    def get_single_project_from_url(self, url):
        '''https://docs.gitlab.com/ee/api/projects.html#get-single-project'''

        parsed_url = urlparse(url)
        request_url = '/projects/{}'.format(
            parsed_url.path[1:].replace('/', '%2F'),
            self.personal_access_token
        )

        json_data = self._get(request_url)
        result = {
            'id': json_data['id'],
            'name': json_data['name'],
            'name_with_namespace': json_data['name_with_namespace'],
            'description': json_data['description'],
            'web_url': json_data['web_url']
        }
        return result

    def list_repository_branches(self, id):
        '''https://docs.gitlab.com/ee/api/branches.html#list-repository-branches'''

        url = f'/projects/{id}/repository/branches'
        json_data = self._get(url)
        result = [branch['name'] for branch in json_data]
        result.sort()
        return result

    def list_project_pipelines(self, id, **kwargs):
        '''https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines'''

        url = f'/projects/{id}/pipelines'

        if len(kwargs):
            url += '?' + '&'.join(
                [f'{key}={value}' for key, value in kwargs.items()]
            )

        json_data = self._get(url)
        return json_data

    def get_single_pipeline(self, id, pipeline_id):
        '''https://docs.gitlab.com/ee/api/pipelines.html#get-a-single-pipeline'''

        url = f'/projects/{id}/pipelines/{pipeline_id}'
        json_data = self._get(url)
        return json_data

    def get_single_commit(self, id, sha):
        '''https://docs.gitlab.com/ee/api/commits.html#get-a-single-commit'''

        url = f'/projects/{id}/repository/commits/{sha}'
        json_data = self._get(url)
        return json_data

    def get_raw_file_from_repository(self, id, file_path, ref=None):
        '''https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository'''

        quoted_file_path = quote(file_path, safe='')
        url = f'/projects/{id}/repository/files/{quoted_file_path}/raw'

        if ref is not None:
            url += f'?ref={ref}'

        data = self._get(url, return_json=False, return_bytes=True)
        return data
