from datetime import datetime

from flask_login import UserMixin

from webhooks import db


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50), unique=True)
    password_hash = db.Column(db.String(80), unique=True)


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(100))
    name_with_namespace = db.Column(db.String(100))
    description = db.Column(db.Text)
    personal_access_token = db.Column(db.String(50))
    web_url = db.Column(db.String(100))
    api_url = db.Column(db.String(100))
    pipeline_hooks = db.relationship(
        'PipelineHook',
        backref='project',
        lazy='dynamic'
    )


class PipelineHook(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    branch = db.Column(db.String(100))
    status = db.Column(db.String(8))
    service = db.Column(db.String(50))
    filename = db.Column(db.String(50))
    download_filename = db.Column(db.String(50))
    version_filename = db.Column(db.String(128))
    secret_token = db.Column(db.String(128))
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    events = db.relationship(
        'PipelineHookEvent',
        backref='pipeline_hook',
        lazy='dynamic'
    )


class PipelineHookEvent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    pipeline_hook_id = db.Column(
        db.Integer,
        db.ForeignKey('pipeline_hook.id'), unique=True
    )
