import os
import xml.etree.ElementTree as ET
from datetime import datetime

from dateutil.parser import parse
from flask import url_for

from webhooks import app
from webhooks.api import GitLabAPI


class RSS:
    '''Class to create RSS v2.0'''

    _date_format = '%a, %d %b %Y %H:%M:%S GMT'

    def __init__(self, title, link, description):
        self.title = title
        self.link = link
        self.description = description
        self.items = []

    def _add_sub_element(self, parrent, tag, text):
        sub_element = ET.SubElement(parrent, tag)
        sub_element.text = text

    def write(self, filename):
        rss = ET.Element('rss', attrib={'version': '2.0'})

        channel = ET.SubElement(rss, 'channel')
        self._add_sub_element(channel, 'title', self.title)
        self._add_sub_element(channel, 'link', self.link)
        self._add_sub_element(channel, 'description', self.description)
        self._add_sub_element(
            channel,
            'lastBuildDate',
            datetime.utcnow().strftime(self._date_format)
        )

        for item in self.items:
            channel_item = ET.SubElement(channel, 'item')
            self._add_sub_element(channel_item, 'title', item.title)
            self._add_sub_element(
                channel_item,
                'description',
                item.description
            )
            for key, value in item.optional_attribs.items():
                if isinstance(value, datetime):
                    self._add_sub_element(
                        channel_item,
                        key,
                        value.strftime(self._date_format)
                    )
                    continue
                self._add_sub_element(channel_item, key, value)

        tree = ET.ElementTree(rss)
        tree.write(filename, encoding='utf-8', xml_declaration=True)

    @classmethod
    def from_file(cls, filename):
        tree = ET.parse(filename)
        root = tree.getroot()

        title = None
        link = None
        description = None
        channel = root.find('channel')
        for child in channel:
            if child.tag == 'title':
                title = child.text
            elif child.tag == 'link':
                link = child.text
            elif child.tag == 'description':
                description = child.text

        if title is None or link is None:
            raise Exception(f'Invalid file format {filename}')

        result = cls(title, link, description)
        items = channel.findall('item')
        for item in items:
            title = None
            description = None
            optional_attribs = {}
            for child in item:
                if child.tag == 'title':
                    title = child.text
                elif child.tag == 'description':
                    description = child.text
                else:
                    if child.tag == 'pubDate':
                        optional_attribs[child.tag] = parse(child.text)
                    else:
                        optional_attribs[child.tag] = child.text

            if title is None or description is None:
                raise Exception(f'Invalid file format {filename}')

            result.items.append(RSSItem(
                title,
                description,
                **optional_attribs
            ))

        return result

    @classmethod
    def from_pipeline_hook(cls, pipeline_hook):
        rss = cls(
            f'Модуль {pipeline_hook.project.name}, ветка {pipeline_hook.branch}',
            url_for('rss', _external=True),
            pipeline_hook.project.description
        )
        return rss


class RSSItem:
    '''Class for working channel.item'''

    def __init__(self, title, description, **kwargs):
        self.title = title
        self.description = description
        self.optional_attribs = kwargs

    @classmethod
    def from_commit_json(cls, commit_json, pipeline_hook=None):
        optional_attribs = {
            'author': commit_json['author_email'],
            'pubDate': parse(commit_json['created_at'])
        }

        download_link = None
        if pipeline_hook is not None:
            download_link = url_for(
                'download_rss',
                pipeline_hook_id=pipeline_hook.id,
                sha=commit_json['id'],
                _external=True
            )

        description = commit_json['message']
        if download_link:
            optional_attribs['guid'] = download_link
            description += '<br><br>Скачать: {}'.format(download_link)

        description = f'<pre>{description}</pre>'

        item = cls(
            commit_json['title'],
            description,
            **optional_attribs
        )

        return item


def create_startup_pipeline_hook_xml(pipeline_hook):
    '''Gets the last pipeline and creates rss from it'''

    project = pipeline_hook.project
    api = GitLabAPI(
        api_url=project.api_url,
        personal_access_token=project.personal_access_token
    )

    request_param = {
        'status': pipeline_hook.status,
        'ref': pipeline_hook.branch
    }

    project_id = project.project_id
    pipelines = api.list_project_pipelines(project_id, **request_param)
    if len(pipelines) == 0:
        return

    rss = RSS.from_pipeline_hook(pipeline_hook)

    count = 0
    for pipeline in pipelines:
        if count == app.config['RSS_ITEMS_COUNT']:
            break

        sha = pipeline['sha']
        commit_json = api.get_single_commit(project_id, sha)

        item = RSSItem.from_commit_json(
            commit_json, pipeline_hook=pipeline_hook
        )
        rss.items.append(item)

        count += 1

    filepath = os.path.join(
        app.instance_path,
        'rss',
        project.name,
        pipeline_hook.branch
    )
    try:
        os.makedirs(filepath)
    except OSError:
        pass

    rss.write(os.path.join(filepath, f'{pipeline_hook.status}_rss.xml'))


def update_pipeline_hook_xml(pipeline_hook, data, token):
    project = pipeline_hook.project

    filename = os.path.join(
        app.instance_path,
        'rss',
        project.name,
        pipeline_hook.branch,
        f'{pipeline_hook.status}_rss.xml'
    )

    if os.path.exists(filename):
        rss = RSS.from_file(filename)
    else:
        rss = RSS.from_pipeline_hook(pipeline_hook)

    commit_json = {
        'author_email': data['commit']['author']['email'],
        'created_at': data['commit']['timestamp'],
        'id': data['commit']['id'],
        'title': data['commit']['message'].split('\n')[0],
        'message': data['commit']['message']
    }

    item = RSSItem.from_commit_json(commit_json, pipeline_hook=pipeline_hook)
    rss.items.insert(0, item)

    while len(rss.items) > app.config['RSS_ITEMS_COUNT']:
        rss.items.pop()

    rss.write(filename)
