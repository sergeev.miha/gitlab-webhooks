import os

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__, instance_relative_config=True)
Bootstrap(app)

from webhooks import config_default as config
app.config.from_object(config)

if not os.path.exists(app.instance_path):
    os.makedirs(app.instance_path)

app.config.from_pyfile('config.py', silent=True)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

import webhooks.routes

__version__ = '0.1'
