import sys

from werkzeug.security import generate_password_hash

from webhooks import db
from webhooks.models import User

if __name__ == "__main__":
    if len(sys.argv) != 3:
        sys.exit()

    new_user = User(
        email=sys.argv[1],
        password_hash=generate_password_hash(sys.argv[2], method='sha256')
    )
    db.session.add(new_user)
    db.session.commit()
